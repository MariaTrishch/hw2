import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  function createFighterProperty(keyValue) {
    const nameElement = createElement({ tagName: 'div', className: 'fighter-preview___info' });
    nameElement.innerText = keyValue.join(': ');

    return nameElement;
  }

  function createImage() {
    const { source, name } = fighter;
    const attributes = {
      src: source,
      title: name,
      alt: name,
    };
    const imgElement = createElement({
      tagName: 'img',
      attributes,
    });
    if (position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }
    imgElement.style.maxHeight = '320px';
    return imgElement;
  }

  if (fighter) {
    const keyValueArrayOfFighterObject = Object.entries(fighter);
    // console.log(keyValueArrayOfFighterObject);
    fighterElement.append(createImage(fighter['source']));
    keyValueArrayOfFighterObject
      .filter((keyValues) => !keyValues.includes('_id') && !keyValues.includes('source'))
      .forEach((keyValue) => fighterElement.append(createFighterProperty(keyValue)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
