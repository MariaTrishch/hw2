import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const bodyElementImg = document.createElement('img');
  bodyElementImg.setAttribute('src', `${fighter.source}`);

  const winnerInfo = {
    title: `${fighter.name} is a winner!`,
    bodyElement: bodyElementImg,
  };

  return showModal(winnerInfo);
}
