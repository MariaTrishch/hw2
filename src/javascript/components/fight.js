import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  // console.log(firstFighter);
  // console.log(secondFighter);
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const healthBars = document.querySelectorAll('.arena___health-bar');
    const healthBarsArr = [...healthBars];
    const playerOne = {
      ...firstFighter,
      healthBar: healthBarsArr[0],
      currentHealth: 100,
      criticalHit: false,
      block: false,
      critHitArr: new Set(),
    };
    const playerTwo = {
      ...secondFighter,
      healthBar: healthBarsArr[1],
      currentHealth: 100,
      criticalHit: false,
      block: false,
      critHitArr: new Set(),
    };

    function attack(attacker, defender) {
      if (attacker.block) {
        console.log(attacker.block);
        defender.block = false;
        return;
      }
      if (defender.block) {
        console.log(defender.block);
        defender.block = false;
        return;
      }
      console.log('critical', defender.criticalHit);
      const totalDamage = getDamage(attacker, defender);
      if (defender.criticalHit) {
        defender.currentHealth = defender.currentHealth - ((totalDamage.toFixed() * 2) / defender.health) * 100;
      }
      defender.currentHealth = defender.currentHealth - (totalDamage.toFixed() / defender.health) * 100;
      if (defender.currentHealth < 0) {
        document.removeEventListener('keydown', handleKeyDown);
        document.removeEventListener('keyup', handleKeyUp);
        resolve(attacker);
      }
      defender.healthBar.style.width = `${defender.currentHealth}%`;
      console.log(totalDamage);
    }

    window.addEventListener('keydown', handleKeyDown);
    function handleKeyDown(e) {
      if (e.code === controls.PlayerOneAttack) {
        attack(playerOne, playerTwo);
      }
      if (e.code === controls.PlayerOneBlock) {
        playerOne.block = true;
      }
      if (e.code === controls.PlayerTwoAttack) {
        attack(playerTwo, playerOne);
      }
      if (e.code === controls.PlayerTwoBlock) {
        playerTwo.block = true;
      }

      if (controls.PlayerOneCriticalHitCombination.includes(e.code)) {
        playerOne.critHitArr.add(e.code);
        attack(playerOne, playerTwo);
      }

      if (controls.PlayerTwoCriticalHitCombination.includes(e.code)) {
        playerTwo.critHitArr.add(e.code);
        attack(playerTwo, playerOne);
      }
    }
    window.addEventListener('keyup', handleKeyUp);
    function handleKeyUp(e) {
      if (playerOne.critHitArr.has(e.code)) {
        playerOne.critHitArr.delete(e.code);
      }

      if (playerTwo.critHitArr.has(e.code)) {
        playerTwo.critHitArr.delete(e.code);
      }
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
